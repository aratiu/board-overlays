# Copyright 2022 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2.

# Initial value just for style purposes.
LINUX_FIRMWARE=""
USE=""

BOARD_COMPILER_FLAGS="-march=bdver4 -mno-fma4 -mno-tbm -mno-xop -mno-lwp"
BOARD_RUSTFLAGS="-Ctarget-cpu=bdver4 -Ctarget-feature=-fma4,-tbm,-xop,-lwp"

CHROMEOS_KERNEL_SPLITCONFIG="chromeos-amd-stoneyridge"
CHROMEOS_KERNEL_ARCH="x86_64"

USE="${USE} kernel-5_15"

# Disable ACPI AC device and use USB PD power supply driver
USE="${USE} -acpi_ac"

# Identify the vendor
USE="${USE} amd_cpu"

# GPU sandboxing needs to be started early as GPU process spawns
# multiple threads.
USE="${USE} gpu_sandbox_start_early"

# Use LLVM amdgpu target when building LLVM.
LLVM_TARGETS="AMDGPU"

# LLVM is required to build the radeon and amdgpu drivers.
USE="${USE} llvm"

# Enable GpuMemoryBuffers backed by dma-bufs.
USE="${USE} native_gpu_memory_buffers"

# Enable drm atomic in chromium.
USE="${USE} drm_atomic"

# Enable SMT by default.
USE="${USE} scheduler_configuration_performance"

# TODO: Remove radeon when amdgpu works by itself
VIDEO_CARDS="-* radeon amdgpu"

# Enable AMDGPU linux firmware for Yellow Carp and GC
LINUX_FIRMWARE="${LINUX_FIRMWARE} amdgpu_yellow_carp amdgpu_gc_10_3_7"

# With amdgpu support as part of kernel
USE="${USE} builtin_fw_amdgpu_yellow_carp builtin_fw_amdgpu_gc_10_3_7"

# Enable Android Extension Pack for amd-mesa
USE="${USE} android_aep"

# Enable fscrypt v2 usage on 5.4+
USE="${USE} direncription_allow_v2"
