# Copyright 2015 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

CHROMEOS_KERNEL_SPLITCONFIG="chromiumos-arm64"

# Add the linker option to fix hardware errata. Refer bug here -
# https://code.google.com/p/chrome-os-partner/issues/detail?id=39170
LDFLAGS="-Wl,--fix-cortex-a53-843419"

# Initial value just for style purposes.
USE=""

USE="${USE} containers"

USE="${USE} legacy_keyboard legacy_power_button"
USE="${USE} -opengl opengles"
USE="${USE} kernel-5_15 device_tree"

USE="${USE} direncryption"

# Disable binary options that are not ported yet.
USE="${USE} -pepper_flash -netflix -internal"

# Enable llvm for mesa.
USE="${USE} llvm"

# Enable NNAPI for testing.
USE="${USE} nnapi"

# Enable hps for testing.
USE="${USE} hps"

# Disable unibuild on this legacy board. This is only for existing (as of Jan 2019)
# boards as all new boards must be unibuild enabled moving forward. Do not copy this
# to new boards as it will be rejected. See
# https://chromium.googlesource.com/chromiumos/platform2/+/HEAD/chromeos-config/README.md
# for further details about chromeos-config, which is the required entry point for
# unified builds.
USE="${USE} -unibuild"

# Use legacy pre-Groot UI for initramfs and init script screens.
USE="${USE} legacy_firmware_ui"

# Include many firmwares as this is a "generic" board and might run on a
# wide variety of platforms with who knows what kind of hardware.
LINUX_FIRMWARE="iwlwifi-all"

VIDEO_CARDS="llvmpipe"

# Include sirenia in test images.
USE="${USE} sirenia"

# No support for zero-copy on virtual machines.
USE="${USE} -video_capture_use_gpu_memory_buffer"

# Enable HW codecs using V4L2 API.
USE="${USE} v4l2_codec"

# Enable fscrypt v2 usage on 5.4+
USE="${USE} direncription_allow_v2"

# Enable VKMS and drm_atomic for compositor testing on VMs
USE="${USE} vkms drm_atomic"

# Set mosys_platform USE flag since it was removed from base arm64 overlays
USE="${USE} mosys_platform_generic"
