From 5c338150ba09c486a5c8724aecabd085baaa757f Mon Sep 17 00:00:00 2001
From: Mattias Nissler <mnissler@chromium.org>
Date: Wed, 27 Oct 2021 13:07:44 +0000
Subject: [PATCH] CHROMIUM: Hack VFIO to allow intel-lpss driver to remain
 bound

The kernel checks VFIO groups for "viability". Intuitively, this
determines whether it is safe to operate hardware devices from
userspace via VFIO. One condition is for all devices in the VFIO group
to not have a (non-trivial) driver loaded. This is necessary to
guarantee memory isolation, since all devices in the IOMMU group can
see all mapped memory, and buffers mapped by a bound kernel driver
would thus become indirectly accessible to userspace.

intel-lpss is the driver that handles PCI serial port hardware on
Intel systems. This change adds it to the allowlist. This is safe
because the driver does not set up any IOMMU mappings, in fact it only
operates on the MMIO registers exposed via PCI BAR 0. Note that the
serial driver technically has the ability to make use of DMA, but
fortunately that is not used in our case (hence calling this change a
hack that is not appropriate to propose upstream).

Change-Id: Ia2a338d2a9c6dcbd13a836ea933a17ca588d7736
---
 drivers/vfio/vfio.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/drivers/vfio/vfio.c b/drivers/vfio/vfio.c
index 5e631c359ef2..ba08477b89b2 100644
--- a/drivers/vfio/vfio.c
+++ b/drivers/vfio/vfio.c
@@ -566,7 +566,7 @@ static struct vfio_device *vfio_group_get_device(struct vfio_group *group,
  * that error notification via MSI can be affected for platforms that handle
  * MSI within the same IOVA space as DMA.
  */
-static const char * const vfio_driver_allowed[] = { "pci-stub" };
+static const char * const vfio_driver_allowed[] = { "pci-stub", "intel-lpss" };
 
 static bool vfio_dev_driver_allowed(struct device *dev,
 				    struct device_driver *drv)
-- 
2.33.0.1079.g6e70778dc9-goog

