# Copyright 2017 The ChromiumOS Authors
# Distributed under the terms of the GNU General Public License v2

USE=""
USE="${USE} dptf"
# Required for alternate kernel LPC access
USE="${USE} cros_ec_mec"

# Google USBPD peripheral firmwares
LINUX_FIRMWARE="cros-pd"

# Add EC logging
USE="${USE} eclog"

# Declare TPM model.
USE="${USE} tpm_slb9670"

# WiFi and BT driver firmware
LINUX_FIRMWARE="${LINUX_FIRMWARE} iwlwifi-7265D ibt-hw"

# The devices from this family are affected by double-extend PCR bug
USE="${USE} double_extend_pcr_issue"

# Enable eMMC tools
USE="${USE} mmc"

# Disable unibuild on this legacy board. This is only for existing (as of Jan 2019)
# boards as all new boards must be unibuild enabled moving forward. Do not copy this
# to new boards as it will be rejected. See
# https://chromium.googlesource.com/chromiumos/platform2/+/HEAD/chromeos-config/README.md
# for further details about chromeos-config, which is the required entry point for
# unified builds.
USE="${USE} -unibuild"

# Enable background blur.
USE="${USE} background_blur"

# Enable VMs
USE="${USE} kvm_host crosvm-gpu virtio_gpu"

# Disabled as quickfix for b/162500057
# Use of drm_atomic on Skl chipset shows performance regresion
#USE="${USE} drm_atomic"

# Use AFDO profiles from big core
AFDO_PROFILE_SOURCE="bigcore"

# Enable Downloadable Content (DLC)
USE="${USE} dlc"

# Enable Downloadable Content (DLC) Test
USE="${USE} dlc_test"
